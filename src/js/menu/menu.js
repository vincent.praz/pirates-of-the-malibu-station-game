let lastPosition = 0;
const posLvl = [0.105, 0.296, 0.43, 0.587, 0.764, 1];
const tl = gsap.timeline();
let lvlToStart = 1;
const btn = $(".btnlevel");

const cookies = Cookies.get();

let level1 = null;
let level2 = null;
let level3 = null;
let level4 = null;
let level5 = null;

const lvl1ScoreP = document.getElementById("scoreLvl1");
const lvl2ScoreP = document.getElementById("scoreLvl2");
const lvl3ScoreP = document.getElementById("scoreLvl3");
const lvl4ScoreP = document.getElementById("scoreLvl4");
const lvl5ScoreP = document.getElementById("scoreLvl5");

if (cookies.lvl1) {
  level1 = cookies.lvl1.replace("[", "").replace("]", "").split(",");

  let difficulty = "Easy";
  lvl1ScoreP
    .getElementsByClassName("board")[0]
    .getElementsByClassName("deathpanel")[0]
    .getElementsByClassName("death")[0].innerText = level1[2];
  if (level1[4] == "false") {
    difficulty = "Hard";
  } else {
    lvl1ScoreP
      .getElementsByClassName("board")[0]
      .getElementsByClassName("help")[0]
      .classList.add("true");
  }
}
if (cookies.lvl2) {
  level2 = cookies.lvl2.replace("[", "").replace("]", "").split(",");
  let difficulty = "Easy";
  lvl2ScoreP
    .getElementsByClassName("board")[0]
    .getElementsByClassName("deathpanel")[0]
    .getElementsByClassName("death")[0].innerText = level2[2];
  if (level2[4] == "false") {
    difficulty = "Hard";
  } else {
    lvl2ScoreP
      .getElementsByClassName("board")[0]
      .getElementsByClassName("help")[0]
      .classList.add("true");
  }
}
if (cookies.lvl3) {
  level3 = cookies.lvl3.replace("[", "").replace("]", "").split(",");
  let difficulty = "Easy";
  lvl3ScoreP
    .getElementsByClassName("board")[0]
    .getElementsByClassName("deathpanel")[0]
    .getElementsByClassName("death")[0].innerText = level3[2];
  if (level3[4] == "false") {
    difficulty = "Hard";
  } else {
    lvl3ScoreP
      .getElementsByClassName("board")[0]
      .getElementsByClassName("help")[0]
      .classList.add("true");
  }
}
if (cookies.lvl4) {
  level4 = cookies.lvl4.replace("[", "").replace("]", "").split(",");
  let difficulty = "Easy";
  lvl4ScoreP
    .getElementsByClassName("board")[0]
    .getElementsByClassName("deathpanel")[0]
    .getElementsByClassName("death")[0].innerText = level4[2];
  if (level4[4] == "false") {
    difficulty = "Hard";
  } else {
    lvl4ScoreP
      .getElementsByClassName("board")[0]
      .getElementsByClassName("help")[0]
      .classList.add("true");
  }
}
if (cookies.lvl5) {
  level5 = cookies.lvl5.replace("[", "").replace("]", "").split(",");
  lvl5ScoreP
    .getElementsByClassName("board")[0]
    .getElementsByClassName("deathpanel")[0]
    .getElementsByClassName("death")[0].innerText = level5[2];
}

// Audios
const menuSong = new Audio(
  "../assets/sounds/Pirate Music - The Jolly Roger.mp3"
);
menuSong.volume -= 0.9;
menuSong.loop = true;

const drawnSword = new Audio("../assets/sounds/Drawn sword.mp3");
drawnSword.volume -= 0.5;

$(document).ready(() => {
  menuSong.play();

  gsap.registerPlugin(MotionPathPlugin);
  gsap.set("#player", {
    xPercent: -50,
    yPercent: -90,
    transformOrigin: "50% 50%",
  });

  if (level5 && level5[1] == "true") {
    $("#level2").removeClass("disable");
    $("#level3").removeClass("disable");
    $("#level4").removeClass("disable");
    $("#level5").removeClass("disable");
    $("#level6").removeClass("disable");
    $("#player").removeClass();
    $("#player").addClass("moveL");
    lastPosition = posLvl[4];
    levelUp(6);
  } else if (level4 && level4[1] == "true") {
    $("#level2").removeClass("disable");
    $("#level3").removeClass("disable");
    $("#level4").removeClass("disable");
    $("#level5").removeClass("disable");
    lastPosition = posLvl[4];
    levelUp(5);
  } else if (level3 && level3[1] == "true") {
    $("#level2").removeClass("disable");
    $("#level3").removeClass("disable");
    $("#level4").removeClass("disable");
    lastPosition = posLvl[2];
    levelUp(4);
  } else if (level2 && level2[1] == "true") {
    $("#level2").removeClass("disable");
    $("#level3").removeClass("disable");
    lastPosition = posLvl[1];
    levelUp(3);
  } else if (level1 && level1[1] == "true") {
    $("#level2").removeClass("disable");
    lastPosition = posLvl[0];
    levelUp(2);
  } else {
    tl.from(".boat", { duration: 2, left: "0%" });

    // gsap.from(".btnlevel", { duration: 1, opacity: 0, stagger: 0.5 });
    tl.to("#player", {
      duration: 3,
      motionPath: {
        path: "#path",
        align: "#path",
        start: lastPosition,
        end: posLvl[0],
      },
    });
    lastPosition = 0.105;
  }
});
function toLevel(lvl) {
  if (lvlToStart == 6) {
    document.getElementById("txtStart").innerText = "Start";
  }
  lvlToStart = lvl;
  if (lvlToStart == 6) {
    document.getElementById("txtStart").innerText = "Treasure";
  }
  lvl -= 1;
  gsap.to("#player", {
    duration: 3,
    immediateRender: true,
    motionPath: {
      path: "#path",
      align: "#path",
      start: lastPosition,
      end: posLvl[lvl],
    },
  });
  lastPosition = posLvl[lvl];
}
function levelUp(lvl) {
  lvlToStart = lvl;
  if (lvl == 6) {
    document.getElementById("txtStart").innerText = "Treasure";
  }
  lvl -= 1;
  tl.to("#player", {
    duration: 3,
    delay: 0.5,
    motionPath: {
      path: "#path",
      align: "#path",
      start: lastPosition,
      end: posLvl[lvl],
    },
  });
  lastPosition = posLvl[lvl];
}
function launch() {
  drawnSword.play();
  $("#btnstart").addClass("go");
  if (lvlToStart != 6) {
    setTimeout(() => {
      window.location.pathname = "/levels/level" + lvlToStart + ".html";
    }, 800);
  } else {
    $("#level6").addClass("open");
    modal.style.display = "block";
  }
}

//______________________________________________________________
// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementById("closeModal");

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
