// ____________ VARIABLES ____________
const G = 10;
const ms = 7;
const Vjump = 10;
const VwallJump = 12;

// Var to know if our player is on the ground
let grounded = false;
const player = document.getElementById("hitboxSword");
const sword = document.getElementById("swordSlash");

// Var for the jump
let jumped;
let reload;
let jumpAction = false;

// Var for the wallJump
let wallJumped;
let wallJumpAction = false;
let wallJumpDirection = "";

// Use this to know what is the current played lvl
const actualLvl = parseInt(document.getElementById("actualLvl").innerText);

// Var to get keys pressed (used in keydown/up)
let keys = {};

// Get all the walls
// let walls = document.getElementsByClassName("wall");
const walls = document.getElementsByClassName("block");
// Get all the platforms
const platforms = document.getElementsByClassName("platform");
// Get all the traps
const traps = document.getElementsByClassName("trap");
// Get all Tentacle
const tentacles = document.getElementsByClassName("tentacle_killer");
// Get all Rocks
const rocks = document.getElementsByClassName("rockFalling");
// Get wave
let wave = document.getElementById("waveTrap");

// Get the Kraken Dead
let krakenDead = document.getElementById("krakenKO");
// Get the Kraken
let kraken = document.getElementById("kraken");

// Get the camera
let camera = document.getElementById("camera");

const MAXBOSSHP = 300;

// The Boss HP Counter
let bossHP = MAXBOSSHP;

// The death counter
let deaths = 0;

// Gripping on wall
let wallGrip = false;

// Playing direction (default right)
let playerDirection = "right";

// Get pos of Start Position
let posDepTop;
let posDepLeft;
let posDepBot;

// Mute
let allMute = false;
let sfxMute = false;
let songMute = false;

// Audio variables
// SFXs
// Jump Audio
const jumpAudio = new Audio("../assets/sounds/jump.mp3");
jumpAudio.volume -= 0.75;

// Jump Audio
const swordAudio = new Audio("../assets/sounds/Sword Slash.mp3");
swordAudio.volume -= 0.75;

// Death Audio
const deathAudio = new Audio("../assets/sounds/oof.mp3");

// Step Audio
const stepAudio = new Audio("../assets/sounds/onestep.mp3");
stepAudio.volume -= 0.8;
stepAudio.loop = true;

// Walgrip Audio
const wallGripAudio = new Audio("../assets/sounds/wallgrip.mp3");
wallGripAudio.volume -= 0.9;
wallGripAudio.loop = true;

// Victory Audio
const victoryAudio = new Audio("../assets/sounds/victory.mp3");
victoryAudio.volume -= 0.9;

// Wind tornado Audio
const tornadoAudio = new Audio("../assets/sounds/wind-tornado.mp3");
tornadoAudio.volume -= 0.7;
tornadoAudio.loop = true;

// Wind tentacle Water Spawn Audio
const tentacleWaterSpawnAudio = new Audio(
  "../assets/sounds/tentacle-water-spawn.mp3"
);
tentacleWaterSpawnAudio.volume -= 0.5;

// Squishy tentacle hit Audio
const tentacleHitAudio = new Audio("../assets/sounds/squishy.mp3");

// Songs
// Level 5 Song
const lvl5Audio = new Audio("../assets/sounds/bossSong.mp3");
// Reduce Volume
lvl5Audio.volume -= 0.95;
// Activate loop
lvl5Audio.loop = true;

// Audio array
const allAudio = [
  jumpAudio,
  wallGripAudio,
  deathAudio,
  stepAudio,
  swordAudio,
  lvl5Audio,
  victoryAudio,
  tentacleHitAudio,
  tornadoAudio,
  tentacleWaterSpawnAudio,
];
const allSFX = [
  jumpAudio,
  swordAudio,
  wallGripAudio,
  deathAudio,
  stepAudio,
  victoryAudio,
  tentacleHitAudio,
  tornadoAudio,
  tentacleWaterSpawnAudio,
];
const allSong = [lvl5Audio];

let cookie = "lvl" + actualLvl;
let levelCookie;
let cookieToSend;

let hitSword = false;
let sworded;

if (Cookies.get(cookie)) {
  levelCookie = JSON.parse(Cookies.get(cookie));
} else {
  levelCookie = [0, false, null];
}

/**
 * plays song depending on level
 */
function playSong() {
  lvl5Audio.play();
}

//____________________________________
// Action to make when our doc is ready
$(document).ready(() => {
  playSong();

  // reposition our player
  scrollBy(-1000000, 0);
  posDepTop = document.getElementById("start").getBoundingClientRect().bottom;
  posDepLeft = document.getElementById("start").getBoundingClientRect().left;
  posDepBot = posDepTop - 62;
  playerStartPos();

  // initialize deathcounter from cookie
  deaths = levelCookie[0];
  let deathCounter = document.getElementById("deathCounter");
  deathCounter.innerText = deaths;
  let record = document.getElementById("record");
  if (levelCookie[2] != null) {
    record.innerText = levelCookie[2];
  } else {
    record.innerText = "???";
  }

  // Play level animations
  playLevel();
});

function preventScroll(event) {
  if (event.key == "ArrowLeft") {
    event.preventDefault();
  }
  if (event.key == "ArrowRight") {
    event.preventDefault();
  }
  if (event.key == " ") {
    event.preventDefault();
  }
}
//keydown -> animation on keyboard detection
function keyDown(event) {
  if (event.key == "q") {
    $(".q").addClass("press");
  }
  if (event.key == "ArrowUp" || event.key == "w" || event.key == "W") {
    event.preventDefault();
    $(".up").addClass("press");
    if (grounded) {
      jumpAction = true;
      jumped = 8;
      reload = 3;

      stepAudio.pause();

      // Reset audio sound
      jumpAudio.pause();
      jumpAudio.currentTime = 0;
      // Play jump sound
      jumpAudio.play();
    }

    if (wallGrip) {
      wallJumpAction = true;
      wallJumped = 9;

      stepAudio.pause();

      // Reset audio sound
      jumpAudio.pause();
      jumpAudio.currentTime = 0;
      // Play jump sound
      jumpAudio.play();
    }
  }
  if (event.key == "e") {
    $(".e").addClass("press");
  }
  if (event.key == "ArrowLeft" || event.key == "a" || event.key == "A") {
    event.preventDefault();
    $(".left").addClass("press");
    $("#playerSword").removeClass();
    $("#playerSword").addClass("moveL");
    $("#playerSword").addClass("play");

    // Set player direction to left
    playerDirection = "left";
  }
  if (event.key == "ArrowDown" || event.key == "s" || event.key == "S") {
    event.preventDefault();
    $(".down").addClass("press");
  }
  if (event.key == "ArrowRight" || event.key == "d" || event.key == "D") {
    event.preventDefault();
    $(".right").addClass("press");
    $("#playerSword").removeClass();
    $("#playerSword").addClass("moveR");
    $("#playerSword").addClass("play");
    // Set player direction to left
    playerDirection = "right";
  }
  if (event.key == " ") {
    $(".space").addClass("press");
    event.preventDefault();

    // Swing sword in the right direction
    if (!hitSword) {
      hitSword = true;
      sworded = 20;

      if (playerDirection == "right") {
        $("#playerSword").addClass("hitR");
      } else {
        $("#playerSword").addClass("hitL");
      }
      swordAudio.currentTime = 0;
      swordAudio.play();
      setTimeout(() => {
        $("#playerSword").removeClass("hitR");
        $("#playerSword").removeClass("hitL");
      }, 400);
    }
  }
}
document.addEventListener("keydown", keyDown);
//keyup
function keyUp(event) {
  if (event.key == "q") {
    $(".q").removeClass("press");
  }
  if (event.key == "ArrowUp" || event.key == "w" || event.key == "W") {
    $(".up").removeClass("press");
  }
  if (event.key == "e") {
    $(".e").removeClass("press");
  }
  if (event.key == "ArrowLeft" || event.key == "a" || event.key == "A") {
    $(".left").removeClass("press");
    $("#playerSword").removeClass("play");
    stepAudio.pause();
  }
  if (event.key == "ArrowDown" || event.key == "s" || event.key == "S") {
    $(".down").removeClass("press");
  }
  if (event.key == "ArrowRight" || event.key == "d" || event.key == "D") {
    $(".right").removeClass("press");
    $("#playerSword").removeClass("play");
    stepAudio.pause();
  }
  if (event.key == " ") {
    $(".space").removeClass("press");
  }
}
document.addEventListener("keyup", keyUp);

// ____________ Déplacement d'un élément ____________

// Call the function moveplayer every 20ms
let frameGame = setInterval(moveplayer, 20);

// Add the key down to "keys"
$(document).on("keydown", function (e) {
  switch (e.keyCode) {
    case 65:
      keys[37] = true;
      break;
    case 83:
      keys[40] = true;
      break;
    case 68:
      keys[39] = true;
      break;
    case 87:
      keys[38] = true;
      break;
    default:
      keys[e.keyCode] = true;
      break;
  }
});

// Remove the key in "keys" when not pressed anymore
$(document).on("keyup", function (e) {
  switch (e.keyCode) {
    case 65:
      delete keys[37];
      break;
    case 83:
      delete keys[40];
      break;
    case 68:
      delete keys[39];
      break;
    case 87:
      delete keys[38];
      break;
    default:
      delete keys[e.keyCode];
      break;
  }
});

// Mute sounds
function mute(array) {
  for (const sound of array) {
    sound.muted = true;
  }
}

// Mute sounds
function unmute(array) {
  for (const sound of array) {
    sound.muted = false;
  }
}

function onClickAllAudioMute() {
  if (!allMute) {
    mute(allAudio);
    allMute = true;
    sfxMute = true;
    songMute = true;
    $("#allAudio").removeClass("fa-volume-up");
    $("#allAudio").addClass("fa-volume-mute");
    $("#musicAudio").addClass("slash");
    $("#fxAudio").addClass("slash");
  } else {
    unmute(allAudio);
    allMute = false;
    sfxMute = false;
    songMute = false;

    $("#allAudio").removeClass("fa-volume-mute");
    $("#allAudio").addClass("fa-volume-up");
    $("#musicAudio").removeClass("slash");
    $("#fxAudio").removeClass("slash");

    // play audio song if bugged
    playSong();
  }
}

function onClickAllSFXMute() {
  if (!sfxMute) {
    mute(allSFX);
    sfxMute = true;
    $("#fxAudio").addClass("slash");
  } else {
    unmute(allSFX);
    sfxMute = false;
    $("#fxAudio").removeClass("slash");
  }
}

function onClickAllSongMute() {
  if (!songMute) {
    mute(allSong);
    songMute = true;
    $("#musicAudio").addClass("slash");
  } else {
    unmute(allSong);
    songMute = false;

    $("#musicAudio").removeClass("slash");

    // play audio song if bugged
    playSong();
  }
}

function onClickBackToMenu() {
  // Go back to menu
  window.location.pathname = "/";
}

// Put our player in his starting position
function playerStartPos() {
  $("#playerSword").css({ top: posDepBot, left: posDepLeft });
}

// Keep camera in the center of our fov
function scrollDivIntoView() {
  document.getElementById("camera").scrollIntoView({
    inline: "center",
  });
}

// Get cookies
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

// Gravity
function gravity() {
  let movementpx = "+=" + G;
  let direction = "40";
  grounded = false;
  for (const wally of walls) {
    if (isCollide(player, wally, direction)) {
      movementpx = "+=0";
      grounded = true;

      // Reset walljump
      wallGrip = false;
      // Reset wallgrip sound effect
      wallGripAudio.pause();
    }
  }
  for (const platform of platforms) {
    if (isCollide(player, platform, direction)) {
      movementpx = "+=0";
      grounded = true;
    }
  }
  $("#playerSword").animate({ top: movementpx }, 0);
}

// Called all the time to make move our player including gravity, jump, movement, ...
function moveplayer() {
  scrollDivIntoView();

  if (!jumpAction && !wallJumpAction) {
    gravity();
  }

  // Avoids player hitting wall and getting stuck in it
  for (const wally of walls) {
    if (isCollide(player, wally)) {
      $("#playerSword").animate({ top: "+=5" }, 0);
    }
  }

  // Check if the player is touching a trap
  for (const trap of traps) {
    if (isCollide(player, trap)) {
      // Increment death counter
      deaths += 1;
      let deathCounter = document.getElementById("deathCounter");
      deathCounter.innerText = deaths;
      // Set cookie for deaths
      levelCookie[0] = deaths;
      cookieToSend = JSON.stringify(levelCookie);
      Cookies.set(cookie, cookieToSend);

      // play oof audio
      deathAudio.play();
      document.getElementById("playerSword").classList.add("dead");
      clearInterval(frameGame);
      document.removeEventListener("keydown", keyDown);
      document.removeEventListener("keyup", keyUp);
      document.addEventListener("keydown", preventScroll);
      // Reset to last checkpoint
      // SCROLL page
      setTimeout(() => {
        frameGame = setInterval(moveplayer, 20);
        playerStartPos();
        document.getElementById("playerSword").classList.remove("dead");
        $("#camera").css({ top: "0", left: "0" });
        $("#playerSword").removeClass("play");
        document.removeEventListener("keydown", preventScroll);
        document.addEventListener("keydown", keyDown);
        document.addEventListener("keyup", keyUp);
      }, 2000);

      // When player dies restart level
      restartLVL();
    }
  }

  // Test to check if our player should jump
  if (hitSword) {
    hit();
  }
  // Test to check if our player should jump
  if (jumpAction) {
    jump();
  }
  // Test to check if our player should walljump
  if (wallJumpAction) {
    wallJump(wallJumpDirection);
  }

  if (grounded) {
    //add if class actuelle move L alors cast L et si moveR alors cast R
    $("#playerSword").removeClass("playJump");
    $("#playerSword").removeClass("castSpellR");
    $("#playerSword").removeClass("castSpellL");
  } else {
    //add if class actuelle move L alors cast L et si moveR alors cast R
    $("#playerSword").addClass("playJump");
    if ($("#playerSword").hasClass("moveR")) {
      $("#playerSword").addClass("castSpellR");
    } else if ($("#playerSword").hasClass("moveL")) {
      $("#playerSword").addClass("castSpellL");
    }
  }

  for (let direction in keys) {
    if (!keys.hasOwnProperty(direction)) continue;
    //left
    if (direction == 37) {
      wallGrip = false;
      wallGripAudio.pause();
      let movementpx = "-=" + ms;
      //add boucle to get all the walls
      for (const wally of walls) {
        //si collision alors déplacement de 0px;
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";
          if (!grounded) {
            // Anti gravity force
            $("#playerSword").animate({ top: "-=7" }, 0);

            // Allow rejump
            wallGrip = true;
            wallGripAudio.play();

            wallJumpDirection = "right";
          }
        } else if (!isCollide(player, camera, direction)) {
          // Camera scrolling
          $("#camera").animate({ left: movementpx }, 0);
        }
      }
      // step Audio
      if (grounded) {
        stepAudio.play();
      } else {
        stepAudio.pause();
      }
      $("#playerSword").animate({ left: movementpx }, 0);
    }
    //right
    if (direction == 39) {
      wallGrip = false;
      wallGripAudio.pause();
      let movementpx = "+=" + ms;
      for (const wally of walls) {
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";

          // Check if the player is not on the ground
          if (!grounded) {
            // Anti gravity force
            $("#playerSword").animate({ top: "-=7" }, 0);

            // Allow rejump
            wallGrip = true;
            wallGripAudio.play();

            wallJumpDirection = "left";
          }
        } else if (!isCollide(player, camera, direction)) {
          // Camera scrolling
          $("#camera").animate({ left: movementpx }, 0);
        }
      }
      // step Audio
      if (grounded) {
        stepAudio.play();
      } else {
        stepAudio.pause();
      }
      $("#playerSword").animate({ left: movementpx }, 0);
    }
    //down
    if (direction == 40) {
      let movementpx = "+=" + ms;
      for (const wally of walls) {
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";
        }
      }
      $("#playerSword").animate({ top: movementpx }, 0);
    }
  }
}

// Detect collision
function isCollide(myplayer, obstacle, direction) {
  let object_1 = myplayer.getBoundingClientRect();
  let object_2 = obstacle.getBoundingClientRect();
  let toReturn;
  switch (direction) {
    case "37": //left
      if (
        // check if the future mouvement of my player will collide with an other ellement identified as a wall
        // by getting the position of both object
        object_1.left - ms < object_2.left + object_2.width &&
        object_1.left - ms + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "38": //up
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top - G < object_2.top + object_2.height &&
        object_1.top - G + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "39": //right
      if (
        object_1.left + ms < object_2.left + object_2.width &&
        object_1.left + ms + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "40": //down
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top + G < object_2.top + object_2.height &&
        object_1.top + G + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    default:
      // Case checking if the player is inside the obstacle
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
  }
}

// Jump
function jump() {
  if (jumped > 0) {
    grounded = false;
    jumped -= 1;
    let movementpx = "-=" + Vjump;
    if (wallGrip) {
      movementpx = "-=" + (Vjump - 7);
      wallGripAudio.pause();
    }
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
        jumped = 0;
        reload = 0;
      }
    }
    $("#playerSword").animate({ top: movementpx }, 0);
  } else if (reload > 0) {
    let movementpx = "-=" + (Vjump - (4 - reload) * 5);
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
      }
    }
    reload -= 1;
    $("#playerSwordSword").animate({ top: movementpx }, 0);
  } else {
    jumpAction = false;
  }
}

// WallJump
function wallJump(wallJumpD) {
  //reset wallgrip
  wallGrip = false;
  wallGripAudio.pause();

  if (wallJumped > 0) {
    wallJumped -= 1;
    let movementpx = "-=" + VwallJump;
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
        wallJumped = 0;
      }
    }

    let msJump = ms;

    switch (wallJumped) {
      case 3:
        msJump -= 2;
        break;
      case 2:
        msJump -= 3;
        break;
      case 1:
        msJump -= 4;
        break;
      case 0:
        msJump -= 5;
        break;
    }

    // Check direction
    let movementDirection = "+=" + msJump;
    if (wallJumpD === "left") {
      movementDirection = "-=" + msJump;
      delete keys[39];
    } else {
      delete keys[37];
    }

    $("#playerSword").animate({ top: movementpx, left: movementDirection }, 0);
  } else {
    wallJumpAction = false;
  }
}

// Hit with sword
function hit() {
  if (sworded > 0) {
    sworded -= 1;

    for (const tentacle of tentacles) {
      if (isCollide(sword, tentacle)) {

        // Play hit audio
        tentacleHitAudio.play();

        // The tentacle is descending
        tentacle.classList.remove("_up");
        // Damaging the boss
        damageBoss();
      }
    }
  } else {
    hitSword = false;
  }
}

// Remove hp boss
function damageBoss() {
  bossHP -= 1;

  // Update health bar
  document.getElementById("health").value = bossHP;

  // If boss dies
  if (bossHP <= 0) {
    clearInterval(frameGame);
    document.removeEventListener("keydown", keyDown);
    document.removeEventListener("keyup", keyUp);
    document.addEventListener("keydown", preventScroll);
    $("#playerSword").removeClass();
    $("#playerSword").addClass("victory");
    kraken.hidden = true;
    krakenDead.hidden = false;
    levelCookie[1] = true;
    if (levelCookie[2] == null || levelCookie[2] > levelCookie[0]) {
      levelCookie[2] = levelCookie[0];
    }
    levelCookie[0] = 0;

    cookieToSend = JSON.stringify(levelCookie);
    Cookies.set(cookie, cookieToSend);

    // Play victory audio
    victoryAudio.play();

    // Return to menu
    setTimeout(() => {
      onClickBackToMenu();
    }, 2000);
  }
}

let tentacleInterval = null;
let rockInterval = null;
let waveInterval = null;
let waveIntervalRemove = null;
// Starts the invervals for the animations
function playLevel() {
  krakenDead.hidden = true;

  tentacleInterval = setInterval(function () {
    spawnTentacle();
  }, 5000);
  rockInterval = setInterval(function () {
    fallRock();
  }, 3000);
  waveInterval = setInterval(function () {
    spawnWave();
  }, 20000);
  waveIntervalRemove = setInterval(function () {
    wave.classList.remove("waveMoving");
    tornadoAudio.pause();
    tornadoAudio.currentTime = 0;
  }, 32000);
}

// Randomly spawns tentacle
function spawnTentacle() {
  // Random tentacle number 0-7
  let tentacleNumber = Math.floor(Math.random() * 8);
  tentacles[tentacleNumber].classList.add("_up");
  setTimeout(() => {
    tentacleWaterSpawnAudio.play();
  }, 1500);
}
function fallRock() {
  // Random rock number 0-5
  let rockNumber = Math.floor(Math.random() * 6);
  rocks[rockNumber].classList.add("play");
  setTimeout(() => {
    rocks[rockNumber].classList.remove("play");
  }, 4000);
}

// Spawns a wave
function spawnWave() {
  wave.classList.add("waveMoving");
  tornadoAudio.play();
}

// Reset lvl
function restartLVL() {
  $(".right").removeClass("press");
  $(".left").removeClass("press");
  $(".up").removeClass("press");
  $(".down").removeClass("press");
  $(".space").removeClass("press");

  let tentacleLeft = document.getElementById("tentacleLeft");
  let tentacleRight = document.getElementById("tentacleRight");

  tentacleLeft.classList.add("animate");
  tentacleRight.classList.add("animate");

  setTimeout(() => {
    tentacleLeft.classList.remove("animate");
    tentacleRight.classList.remove("animate");
  }, 2000);

  // Clear invervals
  clearInterval(tentacleInterval);
  clearInterval(rockInterval);
  clearInterval(waveInterval);
  clearInterval(waveIntervalRemove);
  tornadoAudio.pause();
  tornadoAudio.currentTime = 0;

  // Down all tentacles
  for (const tentacle of tentacles) {
    tentacle.classList.remove("_up");
  }
  // Remove all Rocks
  for (const rock of rocks) {
    rock.classList.remove("play");
  }

  // Clear wave
  wave.classList.remove("waveMoving");

  // Reset boss hp
  bossHP = MAXBOSSHP;

  // Update health bar
  document.getElementById("health").value = bossHP;


  // Resart level animations
  playLevel();
}
