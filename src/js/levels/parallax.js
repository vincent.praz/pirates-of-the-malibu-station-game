$(window).scroll(function () {
  var scroll_position = $(window).scrollLeft();
  $(".parallax1").css({
    "background-position-x": scroll_position / 4 + "px",
  });
  $(".parallax2").css({
    "background-position-x": scroll_position / 2 + "px",
  });
});
