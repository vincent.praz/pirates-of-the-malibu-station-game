// ____________ VARIABLES ____________
const G = 10;
const ms = 7;
const Vjump = 10;
const VwallJump = 12;

// Var to know if our player is on the ground
let grounded = false;
const player = document.getElementById("hitbox");

// Var for the jump
let jumped;
let reload;
let jumpAction = false;

// Var for the wallJump
let wallJumped;
let wallJumpAction = false;
let wallJumpDirection = "";

// Use this to know what is the current played lvl
const actualLvl = parseInt(document.getElementById("actualLvl").innerText);

// Var to get keys pressed (used in keydown/up)
let keys = {};

// Get all the platforms
let easyBlocks = document.getElementsByClassName("plebs");
// Hide easy blocks by default
for (const blocks of easyBlocks) {
  blocks.hidden = true;
}
// checkbox easy
let easyCHCK = document.getElementById("checkboxEZ");

easyCHCK.onclick = function () {
  // access properties using this keyword
  if (this.checked) {
    playerStartPos();
    $("#camera").css({ top: "0", left: "0" });
    for (const blocks of easyBlocks) {
      blocks.hidden = false;
    }
    levelCookie[3] = true;
    cookieToSend = JSON.stringify(levelCookie);
    Cookies.set(cookie, cookieToSend);
  } else {
    playerStartPos();
    $("#camera").css({ top: "0", left: "0" });
    for (const blocks of easyBlocks) {
      blocks.hidden = true;
    }
    levelCookie[3] = false;
    cookieToSend = JSON.stringify(levelCookie);
    Cookies.set(cookie, cookieToSend);
  }
};

// Get all the walls
// let walls = document.getElementsByClassName("wall");
let walls = document.getElementsByClassName("block");
// Get all the platforms
let platforms = document.getElementsByClassName("platform");
// Get all the traps
let traps = document.getElementsByClassName("trap");

// Get the camera
let camera = document.getElementById("camera");

// Finish
let finish = document.getElementById("finish");

// The blocks that are used to launch the buoy indicator
let helpDetector;
// Number of hits required to trigger the buoy indicator
let helpHits = 0;

// The death counter
let deaths = 0;

// Gripping on wall
let wallGrip = false;

// Get pos of Start Position
let posDepTop;
let posDepLeft;
let posDepBot;

// Mute
let allMute = false;
let sfxMute = false;
let songMute = false;

// Audio variables
// SFXs
// Jump Audio
const jumpAudio = new Audio("../assets/sounds/jump.mp3");
jumpAudio.volume -= 0.75;

// Death Audio
const deathAudio = new Audio("../assets/sounds/oof.mp3");

// Spike Audio
const spikeAudio = new Audio("../assets/sounds/spike.mp3");
spikeAudio.volume -= 0.98;

// Step Audio
const stepAudio = new Audio("../assets/sounds/onestep.mp3");
stepAudio.volume -= 0.8;
stepAudio.loop = true;

// Walgrip Audio
const wallGripAudio = new Audio("../assets/sounds/wallgrip.mp3");
wallGripAudio.volume -= 0.9;
wallGripAudio.loop = true;

// Victory Audio
const victoryAudio = new Audio("../assets/sounds/victory.mp3");
victoryAudio.volume -= 0.9;

// Songs

// Level 1 Song
const lvl1Audio = new Audio(
  "../assets/sounds/Pirate Folk Music - Pirate Town.mp3"
);
// Reduce Volume
lvl1Audio.volume -= 0.9;
// Activate loop
lvl1Audio.loop = true;

// Level 2 Song
const lvl2Audio = new Audio("../assets/sounds/Dark Choir Music.mp3");
// Reduce Volume
lvl2Audio.volume -= 0.9;
// Activate loop
lvl2Audio.loop = true;

// Level 3 Song
const lvl3Audio = new Audio("../assets/sounds/Jungle Tribe.mp3");
// Reduce Volume
lvl3Audio.volume -= 0.9;
// Activate loop
lvl3Audio.loop = true;

// Level 4 Song
const lvl4Audio = new Audio("../assets/sounds/Tribal Jungle Music.mp3");
// Reduce Volume
lvl4Audio.volume -= 0.9;
// Activate loop
lvl4Audio.loop = true;

// Level 5 Song
const lvl5Audio = new Audio("../assets/sounds/bossSong.mp3");
// Reduce Volume
lvl5Audio.volume -= 0.9;
// Activate loop
lvl5Audio.loop = true;

// Audio array
const allAudio = [
  jumpAudio,
  wallGripAudio,
  deathAudio,
  spikeAudio,
  stepAudio,
  lvl1Audio,
  lvl2Audio,
  lvl3Audio,
  lvl4Audio,
  lvl5Audio,
];
const allSFX = [
  jumpAudio,
  wallGripAudio,
  deathAudio,
  spikeAudio,
  stepAudio,
  victoryAudio,
];
const allSong = [lvl1Audio, lvl2Audio, lvl3Audio, lvl4Audio, lvl5Audio];

let cookie = "lvl" + actualLvl;
let levelCookie;
let cookieToSend;

if (Cookies.get(cookie)) {
  levelCookie = JSON.parse(Cookies.get(cookie));
} else {
  levelCookie = [0, false, null, false, null];
}

// Retrieve easy cookie
if (levelCookie[3] == true) {
  // display easy blocks
  for (const blocks of easyBlocks) {
    blocks.hidden = false;
  }
  // Check the checkbox buoy
  easyCHCK.checked = true;
} else {
  // UnCheck the checkbox buoy
  easyCHCK.checked = false;
}

/**
 * plays song depending on level
 */
function playSong() {
  // play Song audio
  switch (actualLvl) {
    case 1:
      lvl1Audio.play();
      break;
    case 2:
      lvl2Audio.play();
      break;
    case 3:
      lvl3Audio.play();
      break;
    case 4:
      lvl4Audio.play();
      break;
    case 5:
      lvl5Audio.play();
      break;
  }
}

//____________________________________
// Action to make when our doc is ready
$(document).ready(() => {
  playSong();

  // reposition our player
  scrollBy(-1000000, 0);
  posDepTop = document.getElementById("start").getBoundingClientRect().bottom;
  posDepLeft = document.getElementById("start").getBoundingClientRect().left;
  posDepBot = posDepTop - 62;
  playerStartPos();

  // Spike Audio
  if (actualLvl != 5) {
    setInterval(function () {
      spikeAudio.pause();
      spikeAudio.currentTime = 0;
      spikeAudio.play();
    }, 2000);

    if (actualLvl == 1) {
      helpDetector = document.getElementById("helpDetector");
    }
  }

  // initialize deathcounter from cookie
  deaths = levelCookie[0];
  let deathCounter = document.getElementById("deathCounter");
  deathCounter.innerText = deaths;
  let record = document.getElementById("record");
  if (levelCookie[2] != null) {
    record.innerText = levelCookie[2];
  } else {
    record.innerText = "???";
  }
});

//keydown -> animation on keyboard detection
function keyDown(event) {
  if (event.key == "q") {
    $(".q").addClass("press");
  }
  if (event.key == "ArrowUp" || event.key == "w" || event.key == "W") {
    event.preventDefault();
    $(".up").addClass("press");
    if (grounded) {
      jumpAction = true;
      jumped = 8;
      reload = 3;

      stepAudio.pause();

      // Reset audio sound
      jumpAudio.pause();
      jumpAudio.currentTime = 0;
      // Play jump sound
      jumpAudio.play();
    }

    if (wallGrip) {
      wallJumpAction = true;
      wallJumped = 9;

      stepAudio.pause();

      // Reset audio sound
      jumpAudio.pause();
      jumpAudio.currentTime = 0;
      // Play jump sound
      jumpAudio.play();
    }
  }
  if (event.key == "e") {
    $(".e").addClass("press");
  }
  if (event.key == "ArrowLeft" || event.key == "a" || event.key == "A") {
    event.preventDefault();
    $(".left").addClass("press");
    $("#player").removeClass();
    $("#player").addClass("moveL");
    $("#player").addClass("play");
  }
  if (event.key == "ArrowDown" || event.key == "s" || event.key == "S") {
    event.preventDefault();
    $(".down").addClass("press");
  }
  if (event.key == "ArrowRight" || event.key == "d" || event.key == "D") {
    event.preventDefault();
    $(".right").addClass("press");
    $("#player").removeClass();
    $("#player").addClass("moveR");
    $("#player").addClass("play");
  }
  if (event.key == " ") {
    $(".space").addClass("press");
    event.preventDefault();
  }
}
document.addEventListener("keydown", keyDown);
//
function keyUp(event) {
  if (event.key == "q") {
    $(".q").removeClass("press");
  }
  if (event.key == "ArrowUp" || event.key == "w" || event.key == "W") {
    $(".up").removeClass("press");
  }
  if (event.key == "e") {
    $(".e").removeClass("press");
  }
  if (event.key == "ArrowLeft" || event.key == "a" || event.key == "A") {
    $(".left").removeClass("press");
    $("#player").removeClass("play");
    stepAudio.pause();
  }
  if (event.key == "ArrowDown" || event.key == "s" || event.key == "S") {
    $(".down").removeClass("press");
  }
  if (event.key == "ArrowRight" || event.key == "d" || event.key == "D") {
    $(".right").removeClass("press");
    $("#player").removeClass("play");
    stepAudio.pause();
  }
  if (event.key == " ") {
    $(".space").removeClass("press");
  }
}
document.addEventListener("keyup", keyUp);

// ____________ Déplacement d'un élément ____________

// Call the function moveplayer every 20ms
const frameGame = setInterval(moveplayer, 20);

// Add the key down to "keys"
$(document).on("keydown", function (e) {
  switch (e.keyCode) {
    case 65:
      keys[37] = true;
      break;
    case 83:
      keys[40] = true;
      break;
    case 68:
      keys[39] = true;
      break;
    case 87:
      keys[38] = true;
      break;
    default:
      keys[e.keyCode] = true;
      break;
  }
});

// Remove the key in "keys" when not pressed anymore
$(document).on("keyup", function (e) {
  switch (e.keyCode) {
    case 65:
      delete keys[37];
      break;
    case 83:
      delete keys[40];
      break;
    case 68:
      delete keys[39];
      break;
    case 87:
      delete keys[38];
      break;
    default:
      delete keys[e.keyCode];
      break;
  }
});

// Mute sounds
function mute(array) {
  for (const sound of array) {
    sound.muted = true;
  }
}

// Mute sounds
function unmute(array) {
  for (const sound of array) {
    sound.muted = false;
  }
}

function onClickAllAudioMute() {
  if (!allMute) {
    mute(allAudio);
    allMute = true;
    sfxMute = true;
    songMute = true;
    $("#allAudio").removeClass("fa-volume-up");
    $("#allAudio").addClass("fa-volume-mute");
    $("#musicAudio").addClass("slash");
    $("#fxAudio").addClass("slash");
  } else {
    unmute(allAudio);
    allMute = false;
    sfxMute = false;
    songMute = false;

    $("#allAudio").removeClass("fa-volume-mute");
    $("#allAudio").addClass("fa-volume-up");
    $("#musicAudio").removeClass("slash");
    $("#fxAudio").removeClass("slash");

    // play audio song if bugged
    playSong();
  }
}

function onClickAllSFXMute() {
  if (!sfxMute) {
    mute(allSFX);
    sfxMute = true;
    $("#fxAudio").addClass("slash");
  } else {
    unmute(allSFX);
    sfxMute = false;
    $("#fxAudio").removeClass("slash");
  }
}

function onClickAllSongMute() {
  if (!songMute) {
    mute(allSong);
    songMute = true;
    $("#musicAudio").addClass("slash");
  } else {
    unmute(allSong);
    songMute = false;

    $("#musicAudio").removeClass("slash");

    // play audio song if bugged
    playSong();
  }
}

function onClickBackToMenu() {
  // Go back to menu
  window.location.pathname = "/";
}

// Put our player in his starting position
function playerStartPos() {
  $("#player").css({ top: posDepBot, left: posDepLeft });
}

// Keep camera in the center of our fov
function scrollDivIntoView() {
  document.getElementById("camera").scrollIntoView({
    inline: "center",
  });
}

// Get cookies
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

// Gravity
function gravity() {
  let movementpx = "+=" + G;
  let direction = "40";
  grounded = false;

  // Show help text tutorial on lvl 1 walljump
  if (actualLvl == 1) {
    if (helpHits < 300) {
      if (isCollide(player, helpDetector, direction)) {
        helpHits += 1;
      }
    } else if (helpHits == 300) {
      // Show the buoy indicator
      document.getElementById("popup").classList.add("showHelp");
      helpHits += 1;
    }
  }

  for (const wally of walls) {
    if (isCollide(player, wally, direction)) {
      movementpx = "+=0";
      grounded = true;

      // Reset walljump
      wallGrip = false;
      // Reset wallgrip sound effect
      wallGripAudio.pause();

      if (wally == finish) {
        clearInterval(frameGame);
        document.removeEventListener("keydown", keyDown);
        document.removeEventListener("keyup", keyUp);
        document.addEventListener("keydown", function (event) {
          if (event.key == "ArrowLeft") {
            event.preventDefault();
          }
          if (event.key == "ArrowRight") {
            event.preventDefault();
          }
          if (event.key == " ") {
            event.preventDefault();
          }
        });
        $("#player").removeClass();
        $("#player").addClass("victory");
        levelCookie[1] = true;
        if (levelCookie[2] == null || levelCookie[2] > levelCookie[0]) {
          levelCookie[2] = levelCookie[0];
          levelCookie[4] = levelCookie[3];
        }
        levelCookie[0] = 0;

        cookieToSend = JSON.stringify(levelCookie);
        Cookies.set(cookie, cookieToSend);

        // Play victory audio
        victoryAudio.play();

        // Return to menu
        if (actualLvl == 4) {
          modal.style.display = "block";
        } else {
          setTimeout(() => {
            onClickBackToMenu();
          }, 2000);
        }
      }
    }
  }
  for (const platform of platforms) {
    if (isCollide(player, platform, direction)) {
      movementpx = "+=0";
      grounded = true;
    }
  }
  $("#player").animate({ top: movementpx }, 0);
}

// Called all the time to make move our player including gravity, jump, movement, ...
function moveplayer() {
  scrollDivIntoView();

  if (!jumpAction && !wallJumpAction) {
    gravity();
  }

  for (const wally of walls) {
    if (isCollide(player, wally)) {
      $("#player").animate({ top: "+=5" }, 0);
    }
  }

  // Check if the player is touching a trap
  for (const trap of traps) {
    if (isCollide(player, trap)) {
      // Increment death counter
      deaths += 1;
      let deathCounter = document.getElementById("deathCounter");
      deathCounter.innerText = deaths;
      // Set cookie for deaths
      levelCookie[0] = deaths;
      cookieToSend = JSON.stringify(levelCookie);
      Cookies.set(cookie, cookieToSend);

      // play oof audio
      deathAudio.play();

      // Reset to last checkpoint
      // SCROLL page
      playerStartPos();
      $("#camera").css({ top: "0", left: "0" });
    }
  }

  // Test to check if our player should jump
  if (jumpAction) {
    jump();
  }
  // Test to check if our player should walljump
  if (wallJumpAction) {
    wallJump(wallJumpDirection);
  }

  if (grounded) {
    //add if class actuelle move L alors cast L et si moveR alors cast R
    $("#player").removeClass("playJump");
    $("#player").removeClass("castSpellR");
    $("#player").removeClass("castSpellL");
  } else {
    //add if class actuelle move L alors cast L et si moveR alors cast R
    $("#player").addClass("playJump");
    if ($("#player").hasClass("moveR")) {
      $("#player").addClass("castSpellR");
    } else if ($("#player").hasClass("moveL")) {
      $("#player").addClass("castSpellL");
    }
  }

  for (let direction in keys) {
    if (!keys.hasOwnProperty(direction)) continue;
    //left
    if (direction == 37) {
      wallGrip = false;
      wallGripAudio.pause();
      let movementpx = "-=" + ms;
      //add boucle to get all the walls
      for (const wally of walls) {
        //si collision alors déplacement de 0px;
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";
          if (!grounded) {
            // Anti gravity force
            $("#player").animate({ top: "-=7" }, 0);

            // Allow rejump
            wallGrip = true;
            wallGripAudio.play();

            wallJumpDirection = "right";
          }
        } else if (!isCollide(player, camera, direction)) {
          // Camera scrolling
          $("#camera").animate({ left: movementpx }, 0);
        }
      }
      // step Audio
      if (grounded) {
        stepAudio.play();
      } else {
        stepAudio.pause();
      }
      $("#player").animate({ left: movementpx }, 0);
    }
    //right
    if (direction == 39) {
      wallGrip = false;
      wallGripAudio.pause();
      let movementpx = "+=" + ms;
      for (const wally of walls) {
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";

          // Check if the player is not on the ground
          if (!grounded) {
            // Anti gravity force
            $("#player").animate({ top: "-=7" }, 0);

            // Allow rejump
            wallGrip = true;
            wallGripAudio.play();

            wallJumpDirection = "left";
          }
        } else if (!isCollide(player, camera, direction)) {
          // Camera scrolling
          //   scrollBy(ms, 0);
          $("#camera").animate({ left: movementpx }, 0);
        }
      }
      // step Audio
      if (grounded) {
        stepAudio.play();
      } else {
        stepAudio.pause();
      }
      $("#player").animate({ left: movementpx }, 0);
    }
    //down
    if (direction == 40) {
      let movementpx = "+=" + ms;
      for (const wally of walls) {
        if (isCollide(player, wally, direction)) {
          movementpx = "+=0";
        }
      }
      $("#player").animate({ top: movementpx }, 0);
    }
  }
}

// Detect collision
function isCollide(myplayer, obstacle, direction) {
  let object_1 = myplayer.getBoundingClientRect();
  let object_2 = obstacle.getBoundingClientRect();
  let toReturn;
  switch (direction) {
    case "37": //left
      if (
        // check if the future mouvement of my player will collide with an other ellement identified as a wall
        // by getting the position of both object
        object_1.left - ms < object_2.left + object_2.width &&
        object_1.left - ms + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "38": //up
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top - G < object_2.top + object_2.height &&
        object_1.top - G + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "39": //right
      if (
        object_1.left + ms < object_2.left + object_2.width &&
        object_1.left + ms + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    case "40": //down
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top + G < object_2.top + object_2.height &&
        object_1.top + G + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
    default:
      // Case checking if the player is inside the obstacle
      if (
        object_1.left < object_2.left + object_2.width &&
        object_1.left + object_1.width > object_2.left &&
        object_1.top < object_2.top + object_2.height &&
        object_1.top + object_1.height > object_2.top
      ) {
        toReturn = true;
      } else {
        toReturn = false;
      }
      return toReturn;
  }
}

// Jump
function jump() {
  if (jumped > 0) {
    grounded = false;
    jumped -= 1;
    let movementpx = "-=" + Vjump;
    if (wallGrip) {
      movementpx = "-=" + (Vjump - 7);
      wallGripAudio.pause();
    }
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
        jumped = 0;
        reload = 0;
      }
    }
    $("#player").animate({ top: movementpx }, 0);
  } else if (reload > 0) {
    let movementpx = "-=" + (Vjump - (4 - reload) * 5);
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
      }
    }
    reload -= 1;
    $("#player").animate({ top: movementpx }, 0);
  } else {
    jumpAction = false;
  }
}

// WallJump
function wallJump(wallJumpD) {
  //reset wallgrip
  wallGrip = false;
  wallGripAudio.pause();

  if (wallJumped > 0) {
    wallJumped -= 1;
    let movementpx = "-=" + VwallJump;
    let direction = "38";
    for (const wally of walls) {
      if (isCollide(player, wally, direction)) {
        movementpx = "+=0";
        wallJumped = 0;
      }
    }

    let msJump = ms;

    switch (wallJumped) {
      case 3:
        msJump -= 2;
        break;
      case 2:
        msJump -= 3;
        break;
      case 1:
        msJump -= 4;
        break;
      case 0:
        msJump -= 5;
        break;
    }

    // Check direction
    let movementDirection = "+=" + msJump;
    if (wallJumpD === "left") {
      movementDirection = "-=" + msJump;
      delete keys[39];
    } else {
      delete keys[37];
    }

    $("#player").animate({ top: movementpx, left: movementDirection }, 0);
  } /*else if (wallReload > 0) {
        let movementpx = "-=" + (Vjump - (4 - wallReload) * 5);
        let direction = "38";
        for (const wally of walls) {
            if (isCollide(player, wally, direction)) {
                movementpx = "+=0";
            }
        }
        wallReload -= 1;
        $("#player").animate({top: movementpx}, 0);
    } */ else {
    wallJumpAction = false;
  }
}

//______________________________________________________________
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementById("nextLevel");

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  onClickBackToMenu();
};
