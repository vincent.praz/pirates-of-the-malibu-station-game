"use strict";

// Déclaration des dépendances
// var $ = require('gulp-load-plugins')({lazy:ture});
var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();
var concat = require("gulp-concat");

// Sass utilise le compiler de node.js
sass.compiler = require("node-sass");

// Création de ma tâche pour compiler mes fichier sass en css
gulp.task("sass", function () {
  return gulp
    .src("./src/sass/style.scss")
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./build/css"))
    .pipe(browserSync.stream());
});

gulp.task("scripts1", function () {
  return gulp
    .src("./src/js/levels/*.js")
    .pipe(concat("level.js", { newLine: ";" }))
    .pipe(gulp.dest("./build/js/"))
    .pipe(browserSync.stream());
});
gulp.task("scripts2", function () {
  return gulp
    .src("./src/js/menu/*.js")
    .pipe(concat("menu.js", { newLine: ";" }))
    .pipe(gulp.dest("./build/js/"))
    .pipe(browserSync.stream());
});

gulp.task("scripts3", function () {
  return gulp
    .src("./src/js/boss/*.js")
    .pipe(concat("boss.js", { newLine: ";" }))
    .pipe(gulp.dest("./build/js/"))
    .pipe(browserSync.stream());
});

gulp.task("html", function () {
  return gulp
    .src("./src/**/*.html")
    .pipe(gulp.dest("./build"))
    .pipe(browserSync.stream());
});

gulp.task("watch", function () {
  browserSync.init({
    server: {
      baseDir: "./build",
    },
  });

  gulp.src(["src/assets/**/*"]).pipe(gulp.dest("build/assets"));

  gulp.watch(
    "./src/sass/**/*.scss",
    { ignoreInitial: false },
    gulp.series("sass")
  );
  gulp.watch(
    "./src/js/**/*.js",
    { ignoreInitial: false },
    gulp.series(["scripts1", "scripts2", "scripts3"])
  );
  gulp
    .watch("./src/**/*.html", { ignoreInitial: false }, gulp.series("html"))
    .on("change", browserSync.reload);
});
